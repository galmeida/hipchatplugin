from setuptools import setup

setup(
    name='HipChatPlugin', version='1.0.1',
    packages=['hipchat'],
    entry_points = {
        'trac.plugins': [
            'HipChatPlugin = hipchat',
        ],
    },
)
